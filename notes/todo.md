## todo list
### v2.1.1
 - complete javadoc comments
 - double check tutorial

### v2.1.1
 - documentation
 - tests

### v3.0

### future
 - merging
 	 - JSONMerger ?
 	 - JSONMergeStrategy ?
	 - JSONAdapter.merge( JSONAdapter)
	 - JSONAdapter.merge( JSONArray)
	 - JSONAdapter.merge( JSONObject)
	 - JSONAdapter.merge( Object) ?
 - reference set ( converse of deref )
	 - JSONAdapter.reference( String, Object) ?
	 - JSONAdapter.ref( String, Object) ?
	 - JSONAdapter.setRef( String, Object) ?
 - custom exceptions for each possible failure cases
 - iterator
 - deep ( multi-level ) iterator
 - test above
 - document above
 - complete fork of json-simple ?

## done list
### v2.0
 - finish JSONAdapter.set( String[], Object)
 - commenting

### v2.1
 - JSONAdapter.deref( String)
 - deref casts
 - fix tutorial

### v2.1.1

### v3.0
