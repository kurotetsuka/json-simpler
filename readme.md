## [json-simpler](http://kurotetsuka.github.io/json-simpler/) :: An extension to [json-simple](https://code.google.com/p/json-simple/)

By: [kurotetsuka](https://github.com/kurotetsuka)  
This library is released under the GNU LGPL. See [license.md](license.md) and [gnu-lgpl-v3.0.md](legal/gnu-lgpl-v3.0.md) for details.

------------------------------------------------------------------------

Github: [github.com/kurotetsuka/json-simpler](https://github.com/kurotetsuka/json-simpler/)  
Drone: [drone.io/github.com/kurotetsuka/json-simpler](https://drone.io/github.com/kurotetsuka/json-simpler)  
Site: [kurotetsuka.github.io/json-simpler](http://kurotetsuka.github.io/json-simpler/)  
Javadoc: [kurotetsuka.github.io/json-simpler/doc](http://kurotetsuka.github.io/json-simpler/doc/)  

Latest Release: [2.0](https://github.com/kurotetsuka/json-simpler/releases/tag/v2.0)  
Latest Beta: [2.1b2](https://github.com/kurotetsuka/json-simpler/releases/tag/v2.1b2)  
Latest Alpha: [2.0a1](https://github.com/kurotetsuka/json-simpler/releases/tag/v2.0a1)  
Build Status: [![Build Status](https://drone.io/github.com/kurotetsuka/json-simpler/status.png)](https://drone.io/github.com/kurotetsuka/json-simpler/latest)

